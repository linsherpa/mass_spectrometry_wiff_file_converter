#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow
label: For conversion purpose

inputs:
  in_file:
    type: File
    label: mzXML or vendor file format
    format: edam:format_3245
    secondaryFiles:
      - .scan

  parameters:
    type: string[]
    default: [mzML]

steps:
  step1:
    run: string_extractor.cwl
    in:
      input_file: in_file
    out: [output2]

  step2:
    run: msconvert.cwl
    in:
      in_file: in_file
      in_dir: step1/output2
    out: [outfile, output_location, output_dir, output_string]

  step3:
    run: validation_file_creation.cwl
    in:
      input: step2/output_string
      in_dir: step1/output2
    out: [output_file, output_dir]

outputs:
  outputA:
    type: string
    outputSource: step1/output2

  outputB:
    type: Directory
    outputSource: step2/output_dir

  outputC:
    type: Directory
    outputSource: step3/output_dir


$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
  - https://schema.org/version/latest/schemaorg-current-http.rdf
  - http://edamontology.org/EDAM_1.18.owl
